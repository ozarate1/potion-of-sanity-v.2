import {Potion} from "./Potion.mjs";
import {Ingredients} from "./Ingredients.mjs"; 

import {data} from "./data.mjs";  


const ingredients = Ingredients.from(data);

//console.log(ingredients);


  
//Creamos los ingredientes
// Ingredients.create(data);

// //Creamos pociones
const potion1 = Potion.createFromTwoIngredients("Bear Claws", "Bee", ingredients);
potion1.show();

const potion2 = Potion.createFromTwoIngredients("Chicken's Egg", "Chaurus Eggs", ingredients);
potion2.show();

const potion3 = Potion.createFromTwoIngredients("Chaurus Eggs", "Bleeding Crown", ingredients);
potion3.show();







