//console.log("ingredients.js loaded");

import { Ingredient } from "./Ingredient.mjs";

export class Ingredients
{ 
    //static ELEMENTS;  //Array con objetos de la clase Ingredient
    static POSITIVES = ["Fortify", "Resist", "Cure", "Restore"];
    static NEGATIVES = ["Damage", "Weakness"];

    static HEAL = ["Cure", "Restore"];


    constructor(ingredients)
    {
        this.ingredients = ingredients;
    }

    
    static from(data)
    {
        return new Ingredients(data.ingredients.map(element => new Ingredient(element.name, element.effects, element.value, element.weight)));
    }

    show()
    {
        this.forEach(element => {
            element.show();
        });
    }

    find(name)
    {
        return this.ingredients.find(element => element.isFound(name));
    }

    getCommonEffect(name1, name2)
    {   
        // Objeto de retorno:
        // effect: nombre del efecto en común: Ej: "Weakness to Frost"
        // type: Tipo: Positive / Negative

        console.log("entra1: " + this.ingredients.length);

        const ingredient1 = this.find(name1);
        const ingredient2 = this.find(name2);

        //Modificar
        if (ingredient1 === undefined || ingredient2 === undefined)
        {
            return {effect: "", type: "", error: "Ingredients unknown"};
        }
       
        //Obtenemos los nombres de los efectos
        const effects1 = ingredient1.getEffects();
        const effects2 = ingredient2.getEffects();
        
        

        //Verificamos que haya un efecto en común y lo asignamos a la propiedad effect
        let effect;
        for (let i = 0; i < effects1.length; ++i)
        {
            const effectFound = effects2.find(element2 => effects1[i] === element2);
            if (effectFound != undefined)
            {
                effect = effects1[i];
                break;
            }
                
        }

        //Si no hay efecto en común retornamos
        if (effect === undefined)
            return {effect: "", type: ""};

        const type = this.getTypeOfEffect(effect);

        return {effect, type};
              
    }

    getTypeOfEffect(effect)
    {
        let type;

        const effectShort = effect.split(" ")[0];

        const typePositive = Ingredients.POSITIVES.find(element => element === effectShort);
        if (typePositive  != undefined)
            type = "Positive";

        const typeNegative = Ingredients.NEGATIVES.find(element => element === effectShort);
        if (typeNegative  != undefined)
            type = "Negative";
    
        return type;

    }


}