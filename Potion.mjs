import {Ingredients} from "./Ingredients.mjs";

//console.log("potion.js loaded");

export class Potion
{
    
    constructor(name, value, weight, time)
    {
        this.name      = name;
        this.value     = value;       
        this.weight    = weight;
        this.time      = time;
        
    }

    //Creamos una poción a partir de 2 nombres
    static createFromTwoIngredients(name1, name2, ingredients)
    {
        //Obtenemos el efecto en común
        const effectObj = ingredients.getCommonEffect(name1, name2);

        //Caso de ingredientes desconocidos
        if (effectObj.error !== undefined)
            throw new Error(effectObj.error);


        const effectName = effectObj.effect;
        const effectType = effectObj.type;

        console.log("tipo: " + effectType);

        let name;
        let weight;
        let value;
        let time;

        if (effectName != "") //Existe poción
        {
            const potionType = Potion.getType(effectType);
            name = `${potionType} of ${effectName}`;

            if (Ingredients.HEAL.some(effect => effectName.includes(effect)))
            {
                name += `. It heals a character`;
            }

            weight = 10;
            value = 10;
            time = 10;
            
        }
        else  //Fallo al crear la poción
        {
            name = `Failed to create a potion.`;
            weight = 0;
            value = 0;
            time = 0;
        }


        return new Potion(name, value, weight, time)

    }

    static getType(effect)
    {
        const poisonType =  effect === "Positive" ? "Potion" : 
                            effect === "Negative" ? "Poison" : "Unknown";

        if (poisonType !== "Unknown")
            return poisonType;
        else
            throw new Error("Unknown effect type");
    }

    show()
    {

        console.log(`${this.name}`);
        console.log(`Value:         ${this.value}`);
        console.log(`Weight:        ${this.weight}`);
        console.log(`Time:          ${this.time}`);
        console.log(`------------------------------`);
        
    }
   
}