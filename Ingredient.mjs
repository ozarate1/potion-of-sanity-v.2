
//console.log("ingredient.js loaded");

export class Ingredient
{
    
    constructor(name, effects, value, weight)
    {
        this.name = name;
        this.effects = effects;
        this.value = value;       
        this.weight = weight;
        
    }

    isFound(name)
    {
        return this.name === name;
    }

    getEffects()
    {
        return this.effects;
    }

    show()
    {

        console.log(`Ingredient:    ${this.name}`);
        console.log(`Effects:       ${this.effects}`);
        console.log(`Value:         ${this.value}`);
        console.log(`Weight:        ${this.weight}`);
        console.log(`------------------------------`);
        
    }
}

